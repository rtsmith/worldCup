import Vuex from 'vuex'
import axios from 'axios'

const createStore = () => {
  return new Vuex.Store({
    state: {
      core: {
        fixtures: [],
        teams: []
      }
    },
    mutations: {
      setCore(state, data) {
        state.core = data
      }
    },
    actions: {
      async fetchCore(context) {
        let { data } = await axios.get('http://localhost:3000/')
        context.commit('setCore', data)
      }
    },
    getters: {
      getTeam: (state) => (id) => {
        return state.core.teams.find(team => team.id == id)
      },
      getTeamsFixtures: (state) => (id) => {
        console.log(state)
        return state.core.fixtures.filter((fixture) => {
          return fixture.home_team_id == id || fixture.away_team_id == id
        })
      }
    }
  })
}

export default createStore
